Não rola um processador de pagamentos numa conta de débito?
12:12 PM · 7 de jan de 2024
·
462
 Visualizações

Lucas Cavalcanti
@lucascs
·
7 de jan
Tipo com depósitos e retiradas, mas com algumas contas específicas tendo high throughput. Daí só tem que ver quais métricas, tipo disponibilidade, latência e consistência
zanfranceschi
@zanfranceschi
·
7 de jan
Mas sua sugestão seria uma rinha com esse tema específico, né? Nada especificamente com locks, certo?
zanfranceschi
@zanfranceschi
·
7 de jan
Rolar rola, mas a questão é qual seria a Rinha considerando esse tema de pessimismo vs otimismo?

-----------------------

Dado um cenário de operações concorrentes, alta vazão e por uma duração fixa, o objetivo poderia ser ter o menor número de inconsistências no banco. Cada um poderia utilizar uma estratégia de locking.

Seria como a rinha anterior, mas em vez de mais vazão, seria mais consistência
12:28 PM · 7 de jan de 2024
·
301
 Visualizações

Lucas Cavalcanti
@lucascs
·
7 de jan
Isso, a concorrência vai forçar inconsistências se a pessoa não usar lock, daí vc mede que não teve nenhuma inconsistência (e.g saldo negativo ou saldo que não seja a soma das operações) e vê a latência e disponibilidade. Daí compara quem fez otimista VS pessimista
cataploft
@cataploft
·
7 de jan
pensei em algo parecido, fazer operação pra caralho e contar os erros

-----------------------------------


zanfranceschi
@zanfranceschi
Desisti de fazer a 
@rinhadebackend
 com esse tema de lock pessimista/otimista. O assunto é interessante, mas não consegui pensar numa rinha legal com ele.

Entretanto, no link tem o esboço do que seria essa rinha – tem um modelo de banco e simulação.

https://gist.github.com/zanfranceschi/76b74da7883189718841c5f48fd0ed0c
Comentar
zanfranceschi
@zanfranceschi
·
25 de dez de 2023
Deu vontade de transformar isso numa edição 2 da @rinhadebackend pra que quem não manja muito ter uma oportunidade mais prática de aprender sobre o assunto.

Será que vale? twitter.com/zanfranceschi/…
3:04 AM · 7 de jan de 2024
·
7.618
 Visualizações

Lucas Cavalcanti
@lucascs
·
7 de jan
Não rola um processador de pagamentos numa conta de débito?
zanfranceschi
@zanfranceschi
·
7 de jan
Rolar rola, mas a questão é qual seria a Rinha considerando esse tema de pessimismo vs otimismo?
